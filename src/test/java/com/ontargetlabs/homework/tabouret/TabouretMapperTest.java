package com.ontargetlabs.homework.tabouret;

import com.ontargetlabs.homework.chair.Chair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TabouretMapperTest {
    private TabouretMapper tabouretMapper = new TabouretMapper();

    @Test
    void shouldConvertEntityListToDtoList() {
        List<Chair> armchairList = new ArrayList<>();
        armchairList.add(new Tabouret("One"));

        List<TabouretDto> result = tabouretMapper.entityListToDtoList(armchairList);
        TabouretDto firstEntry = result.get(0);

        assertEquals(1, result.size());
        assertEquals("One", firstEntry.getName());
    }
}