package com.ontargetlabs.homework.chair;

import com.ontargetlabs.homework.armchair.ArmchairDto;
import com.ontargetlabs.homework.armchair.ArmchairMapper;
import com.ontargetlabs.homework.tabouret.TabouretDto;
import com.ontargetlabs.homework.tabouret.TabouretMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class ChairServiceTest {

    private ChairRepository chairRepository = Mockito.mock(ChairRepository.class);
    private ArmchairMapper armchairMapper = Mockito.mock(ArmchairMapper.class);
    private TabouretMapper tabouretMapper = Mockito.mock(TabouretMapper.class);
    private ChairService chairService = new ChairService(chairRepository, armchairMapper, tabouretMapper);

    @Test
    void shouldReturnAllArmchairsDtoList() {
        List<Chair> armchairs = new ArrayList<>();
        List<ArmchairDto> armchairDtos = new ArrayList<>();
        armchairDtos.add(new ArmchairDto(1L, "One"));

        Mockito.when(chairRepository.findAll()).thenReturn(armchairs);
        Mockito.when(armchairMapper.entityListToDtoList(armchairs)).thenReturn(armchairDtos);

        List<ArmchairDto> result = chairService.getAllArmchairs();
        ArmchairDto firstEntry = result.get(0);

        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(1L, firstEntry.getId());
        Assertions.assertEquals("One", firstEntry.getName());
    }

    @Test
    void shouldReturnAllTabouretDtoList() {
        List<Chair> armchairs = new ArrayList<>();
        List<TabouretDto> tabouretDtos = new ArrayList<>();
        tabouretDtos.add(new TabouretDto(1L, "One"));

        Mockito.when(chairRepository.findAll()).thenReturn(armchairs);
        Mockito.when(tabouretMapper.entityListToDtoList(armchairs)).thenReturn(tabouretDtos);

        List<TabouretDto> result = chairService.getAllTabourets();
        TabouretDto firstEntry = result.get(0);

        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(1L, firstEntry.getId());
        Assertions.assertEquals("One", firstEntry.getName());
    }
}