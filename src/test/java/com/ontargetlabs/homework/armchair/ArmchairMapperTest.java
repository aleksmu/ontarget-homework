package com.ontargetlabs.homework.armchair;

import com.ontargetlabs.homework.chair.Chair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArmchairMapperTest {
    private ArmchairMapper armchairMapper = new ArmchairMapper();

    @Test
    void shouldConvertEntityListToDtoList() {
        List<Chair> armchairList = new ArrayList<>();
        armchairList.add(new Armchair("One"));

        List<ArmchairDto> result = armchairMapper.entityListToDtoList(armchairList);
        ArmchairDto firstEntry = result.get(0);

        assertEquals(1, result.size());
        assertEquals("One", firstEntry.getName());
    }
}