package com.ontargetlabs.homework.armchair;

import com.ontargetlabs.homework.chair.Chair;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "armchair")
@DiscriminatorValue("armchair")
public class Armchair extends Chair {
    public Armchair(String name) {
        super(name);
    }

    public Armchair() {
    }
}
