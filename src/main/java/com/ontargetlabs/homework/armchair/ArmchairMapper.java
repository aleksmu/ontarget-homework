package com.ontargetlabs.homework.armchair;

import com.ontargetlabs.homework.chair.Chair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ArmchairMapper {
    public List<ArmchairDto> entityListToDtoList(List<Chair> entityList) {
        List<ArmchairDto> dtos = new ArrayList<>();
        entityList.forEach(entity -> dtos.add(entityToDto(entity)));
        return dtos;
    }

    private ArmchairDto entityToDto(Chair entity) {
        return new ArmchairDto(entity.getId(), entity.getName());
    }
}
