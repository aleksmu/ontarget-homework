package com.ontargetlabs.homework.armchair;

import com.ontargetlabs.homework.chair.ChairDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArmchairDto extends ChairDto {
    public ArmchairDto(Long id, String name) {
        super(id, name);
    }
}
