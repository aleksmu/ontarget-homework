package com.ontargetlabs.homework.postconstruct;

import com.ontargetlabs.homework.armchair.Armchair;
import com.ontargetlabs.homework.chair.Chair;
import com.ontargetlabs.homework.chair.ChairRepository;
import com.ontargetlabs.homework.tabouret.Tabouret;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class AddDataToDatabase {
    private final ChairRepository chairRepository;

    @PostConstruct
    private void addInitData() {
        Chair one = new Armchair("Armchair one");
        Chair two = new Armchair("Armchair two");
        Chair three = new Armchair("Armchair three");
        chairRepository.save(one);
        chairRepository.save(two);
        chairRepository.save(three);

        Chair four = new Tabouret("Tabouret one");
        Chair five = new Tabouret("Tabouret two");
        chairRepository.save(four);
        chairRepository.save(five);
    }
}
