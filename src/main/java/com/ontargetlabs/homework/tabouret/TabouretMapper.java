package com.ontargetlabs.homework.tabouret;

import com.ontargetlabs.homework.chair.Chair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TabouretMapper {
    public List<TabouretDto> entityListToDtoList(List<Chair> entityList) {
        List<TabouretDto> dtos = new ArrayList<>();
        entityList.forEach(entity -> dtos.add(entityToDto(entity)));
        return dtos;
    }

    private TabouretDto entityToDto(Chair entity) {
        return new TabouretDto(entity.getId(), entity.getName());
    }
}
