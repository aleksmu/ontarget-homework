package com.ontargetlabs.homework.tabouret;

import com.ontargetlabs.homework.chair.ChairDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TabouretDto extends ChairDto {
    public TabouretDto(Long id, String name) {
        super(id, name);
    }
}
