package com.ontargetlabs.homework.tabouret;

import com.ontargetlabs.homework.chair.Chair;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tabouret")
@DiscriminatorValue("tabouret")
public class Tabouret extends Chair {
    public Tabouret(String name) {
        super(name);
    }

    public Tabouret() {
    }
}
