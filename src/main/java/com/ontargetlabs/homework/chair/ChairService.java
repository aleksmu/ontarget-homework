package com.ontargetlabs.homework.chair;

import com.ontargetlabs.homework.armchair.ArmchairDto;
import com.ontargetlabs.homework.armchair.ArmchairMapper;
import com.ontargetlabs.homework.tabouret.TabouretDto;
import com.ontargetlabs.homework.tabouret.TabouretMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@RequiredArgsConstructor
@Transactional
public class ChairService {
    private final ChairRepository chairRepository;
    private final ArmchairMapper armchairMapper;
    private final TabouretMapper tabouretMapper;

    public List<ArmchairDto> getAllArmchairs() {
        List<Chair> armchairList = chairRepository.findAllArmchairs();
        return armchairMapper.entityListToDtoList(armchairList);
    }

    public List<TabouretDto> getAllTabourets() {
        List<Chair> tabouretList = chairRepository.findAllTabourets();
        return tabouretMapper.entityListToDtoList(tabouretList);
    }
}
