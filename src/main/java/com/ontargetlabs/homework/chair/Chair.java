package com.ontargetlabs.homework.chair;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "chair")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type")
@Getter
public abstract class Chair {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    public Chair(String name) {
        this.name = name;
    }

    public Chair() {
    }
}
