package com.ontargetlabs.homework.chair;

import com.ontargetlabs.homework.armchair.ArmchairDto;
import com.ontargetlabs.homework.tabouret.TabouretDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ChairController {
    private final ChairService chairService;

    @GetMapping("/armchair")
    public Iterable<ArmchairDto> getAllArmchairs() {
        return chairService.getAllArmchairs();
    }

    @GetMapping("/tabouret")
    public Iterable<TabouretDto> getAllTabourets() {
        return chairService.getAllTabourets();
    }
}
