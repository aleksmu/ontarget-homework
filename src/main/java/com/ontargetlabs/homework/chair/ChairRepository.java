package com.ontargetlabs.homework.chair;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChairRepository extends JpaRepository<Chair, Long> {

    @Query(value = "SELECT * FROM chair WHERE type='armchair'", nativeQuery = true)
    List<Chair> findAllArmchairs();

    @Query(value = "SELECT * FROM chair WHERE type='tabouret'", nativeQuery = true)
    List<Chair> findAllTabourets();
}
