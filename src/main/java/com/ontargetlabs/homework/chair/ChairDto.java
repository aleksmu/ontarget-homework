package com.ontargetlabs.homework.chair;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.ontargetlabs.homework.armchair.Armchair;
import com.ontargetlabs.homework.tabouret.Tabouret;
import lombok.Getter;
import lombok.Setter;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Armchair.class, name = "armchair"),
        @JsonSubTypes.Type(value = Tabouret.class, name = "tabouret")
})
@Getter
@Setter
public abstract class ChairDto {
    private Long id;
    private String name;

    public ChairDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
