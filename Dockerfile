FROM openjdk:8-jdk-alpine
MAINTAINER demotest.org
VOLUME /tmp
EXPOSE 8080
ADD build/libs/homework-0.0.1-SNAPSHOT.jar homework.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/homework.jar"]