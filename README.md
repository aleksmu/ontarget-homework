# README #

Homework for ontargetlabs from Aleksandrs Musaluks.

### What is this? ###

Backend part of homework.

### Technology stack ###

* Java
* Postgres
* JUnit
* Docker

### How do I get set up? ###
This project uses docker container to run application with database

* Make sure you have docker installed
* Run following script from this repository:

**`docker-compose up`**

* You may need to run this script first if getting error message

**`docker-machine start default`**